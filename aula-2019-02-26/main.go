package main

import ( 
	"net/http" 
	"fmt"
)

func main() {

	// Declare and assignment:
	server := http.Server{Addr: "172.22.51.136:8080"}

	// Use Handler to print the full URL path:
	http.HandleFunc("/request-info", handler)

	// Init server:
	server.ListenAndServe()
}

func handler(resp http.ResponseWriter, req *http.Request) {

	if req.Method == "GET" {
		fmt.Fprintf(resp, "O Path é: %s\n\n", req.Host + req.URL.String())
		fmt.Fprintf(resp, "O Header é: %s", req.Header)
	}
	// else { resp.Write([]byte("Tu é burro? A aula é sobre receive requests...")) }
}